// Fill out your copyright notice in the Description page of Project Settings.


#include "AITankController.h"
#include "Tank.h"

void AAITankController::BeginPlay()
{
    Super::BeginPlay();
    auto playerTank = GetControlledTank();
    if(!playerTank)
    {
        UE_LOG(LogTemp, Warning, TEXT("AIController Not possessing tank!! AAH!"))
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("AIController possessing %s"), *playerTank->GetName());
    }
    GetPlayerTank();
}

void AAITankController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if(GetPlayerTank())
    {
        GetControlledTank()->AimAt(GetPlayerTank()->GetActorLocation());        
    }
}

ATank* AAITankController::GetControlledTank() const
{
    return Cast<ATank>(GetPawn());
}

ATank* AAITankController::GetPlayerTank() const
{
    auto player = GetWorld()->GetFirstPlayerController()->GetPawn();
    if(!player) {return nullptr;}
    return Cast<ATank>(player);
}