// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerTankController.h"
#include "Tank.h"

void APlayerTankController::BeginPlay()
{
    Super::BeginPlay();
    auto playerTank = GetControlledTank();
    if(!playerTank)
    {
        UE_LOG(LogTemp, Warning, TEXT("PLayController Not possessing tank!! AAH!"))
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("PlayerController possessing %s"), *playerTank->GetName());
    }
}

void APlayerTankController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    AimTowardsCrosshair();

}

void APlayerTankController::AimTowardsCrosshair()
{
    if(!GetControlledTank()) {return;}

    FVector HitLocation;
    if(GetSightRayHitLocation(HitLocation))
    {
        GetControlledTank()->AimAt(HitLocation);   
    }
}

ATank* APlayerTankController::GetControlledTank() const
{
    return Cast<ATank>(GetPawn());
}

bool APlayerTankController::GetSightRayHitLocation(FVector& OutHitLocation) const
{
    int32 ViewportSizeX, ViewportSizeY;
    GetViewportSize(ViewportSizeX, ViewportSizeY);
    FVector2D ScreenLocation = FVector2D(
        ViewportSizeX * CrossHairXLocation, 
        ViewportSizeY * CrossHairYLocation
        );

    FVector LookDirection;
    if(GetLookDirection(ScreenLocation, LookDirection))
    {
        GetLookVectorHitLocation(LookDirection, OutHitLocation);
    }


    auto playerTank = GetControlledTank();
    if(!playerTank) {return false;}
    
    return true;
}

bool APlayerTankController::GetLookDirection(FVector2D ScreenLocation, FVector& OutLookDirection) const
{
    FVector CameraWorldLocation;
    FVector WorldDirection;
    return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraWorldLocation, OutLookDirection);
}

bool APlayerTankController::GetLookVectorHitLocation(FVector LookDirection, FVector& OutHitLocation) const
{
    FHitResult HitResult;
    auto StartLocation = PlayerCameraManager->GetCameraLocation();
    auto EndLocation = StartLocation + (LookDirection * LineTraceRange);
    if(GetWorld()->LineTraceSingleByChannel(
        HitResult,
        StartLocation,
        EndLocation,
        ECollisionChannel::ECC_Visibility
    ))
    {
        OutHitLocation = HitResult.Location;
        return true;
    }
    return false;
}
